<?php
/**
 * Home Page Template
*/

 global $woo_options, $post; 
 get_header();

 if ( is_paged() ) $is_paged = true; else $is_paged = false;
 
 $page_template = woo_get_page_template();
?>

    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div id="content" class="col-full magazine">
    
        <div id="main-sidebar-container">

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <div id="main">                
                <?php get_template_part( 'loop', 'archive' ); ?> 
            </div><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>
            
        </div><!-- /#main-sidebar-container -->         

        <?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->
    <?php woo_content_after(); ?>
    
        
<?php get_footer(); ?>
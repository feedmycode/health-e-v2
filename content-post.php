<?php
/**
 * Post Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `single.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
 global $woo_options;
 
$title_before = '<h1 class="title">';
$title_after = '</h1>';


//Get the current post format and add as tags

$post_formats = get_post_format();
if( false != $post_formats ) {
	if( is_array() ){
		$post_formats = implode( ' ', $post_formats );
	}
}else{
	$post_formats = '';
}

if ( 'video' == $post_formats ) {
	$link_class = '<i class="fa fa-film"></i>';
} else if ( 'audio' == $post_formats ) {
	$link_class = '<i class="fa fa-music"></i>';
} else {	
	//if standard posts needs one.
	$link_class = '';
}
 
if ( ! is_single() ) {

	$title_before = '<h2 class="title">';
	$title_after = '</h2>';

	$title_before = $title_before .$link_class. '<a href="' . get_permalink( get_the_ID() ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
	$title_after = '</a>' . $title_after;

}
 
$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );
 
woo_post_before();
?>
<div <?php post_class(); ?>>
<?php
	woo_post_inside_before();	
	if ( $woo_options['woo_post_content'] != 'content' AND !is_singular() )
		woo_image( 'width='.$woo_options['woo_thumb_w'].'&height='.$woo_options['woo_thumb_h'].'&class=thumbnail '.$woo_options['woo_thumb_align'] );
	the_title( $title_before, $title_after );
	woo_post_meta();
?>
	<div class="entry">
	    <?php
	    	if ( $woo_options['woo_post_content'] == 'content' || is_single() ) { 
					if ( $post->post_excerpt ) { echo '<div class="pf-content"><p><strong>' . get_the_excerpt(). '</strong></p></div>'; }
					 
						the_content(__('Continue Reading &rarr;', 'woothemes') );

				} else {
					the_excerpt();
				}
	    	if ( $woo_options['woo_post_content'] == 'content' || is_singular() ) wp_link_pages( $page_link_args );	    	
	    ?>	    
	</div><!-- /.entry -->
		<?php if ( is_single() ) : ?>
		    
			<?php
			$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'exclude' => get_post_thumbnail_id() );

			$attachments = get_posts($args);
			if ($attachments) { ?>
			    <div class="attachments">				 	
				 	<?php $count == 0; ?>				 	
				 	<?php foreach ( $attachments as $attachment ) { ?>				 		
				 		<?php $type = get_post_mime_type( $attachment->ID ); ?>
				 		<?php if ( $type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png' /*|| $type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || $type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'*/) continue; ?>
				 		<?php $count++; ?>
				 		<?php if ( $count == 1 ) { ?>
				 			<h3>Attachments</h3>
					 		<ul>					 		
				 		<?php } ?>
				    	<li>
				    		<?php // print_r($type); ?>
				    		<div id="attachment-icon"><?php attachment_icon( $type ); ?></div>
				    		<p><?php the_attachment_link( $attachment->ID , false ); ?></p>
				    		<?php 
				    		// echo $type;
				    		if ( $type == "audio/mpeg" ) { ?>
				    			<audio controls>
				    				<source src="<?php echo wp_get_attachment_url( $attachment->ID ) ?>" />
				    				<embed src="<?php echo wp_get_attachment_url( $attachment->ID ) ?>" autostart="false" loop="false" hidden="false" />
				    			</audio>
				    		<?php };
				    		if ( $type == "video/quicktime" || $type == "video/avi" || $type == "video/asf" || $type == "video/mp4" ) { ?>
				    			<video controls>
				    				<source src="<?php echo wp_get_attachment_url( $attachment->ID ) ?>" />
				    				<object type="video/x-ms-wmv" data="<?php echo wp_get_attachment_url( $attachment->ID ) ?>" width="340" height="280">
				    				  <param name="src" value="<?php echo wp_get_attachment_url( $attachment->ID ) ?>" />
				    				  <param name="controller" value="true" />
				    				  <param name="autostart" value="false" />
				    				</object>	
				    			</video>				    				
				    		<?php }; ?>
							<div class="fix"></div>
				    	</li>
				 	<?php } ?>
				 	</ul>
			 	</div>
			<?php } ?>		    			
		<?php endif ?>
	<div class="fix"></div>
<?php
	woo_post_inside_after();
	// if ( is_single() )
	// 	echo related_content( 'clinic', 'district', 5, 'Related Clinics' );
?>
</div><!-- /.post -->
<?php
	woo_post_after();
	$comm = $woo_options[ 'woo_comments' ];
	if ( ( $comm == 'post' || $comm == 'both' ) && is_single() ) { comments_template(); }
?>
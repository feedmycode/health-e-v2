<?php
/**
 * Template Name: Links
 *
 *
 * @package WooFramework
 * @subpackage Template
 */


function scrollablemenu_script() {
    wp_register_script( 'scrollablemenu_custom_script', get_stylesheet_directory_uri() . '/js/scrollablemenu.js' );
    wp_enqueue_script( 'scrollablemenu_custom_script' );
}
add_action( 'wp_enqueue_scripts', 'scrollablemenu_script' );

function scrollablemenu_style() {
    wp_register_style( 'scrollablemenu_custom_style', get_stylesheet_directory_uri() . '/css/scrollablemenu.css' );
    wp_enqueue_style( 'scrollablemenu_custom_style' );
}
add_action( 'wp_enqueue_scripts', 'scrollablemenu_style' );

get_header();
?>
       
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">                     
<?php
	woo_loop_before();
	
	if (have_posts()) { $count = 0;
		while (have_posts()) { the_post(); $count++;
			woo_get_template_part( 'content', 'page' ); // Get the page content template file, contextually.


		}
	}

	woo_loop_after();
?>     
				<!-- scrollable menu -->
				<div id="scrollablemenu">
				<?php $cats = get_categories( "taxonomy=link_category&hierarchical=0" );
				foreach ($cats as $cat) {
				echo '<a href="#' . $cat->cat_name . '" class="scrollablemenubutton">' . $cat->cat_name . '</a>';
				}
				?>
				</div><!-- end of scrollable menu -->
				<div class="fix"></div>
				<!-- link categories -->
				<div class="linkcategories">
				<?php $cats = get_categories( "taxonomy=link_category&hierarchical=0" );
				foreach ( $cats as $cat ) {
				echo '<a name="' . $cat->cat_name . '"></a><h3>' . $cat->cat_name . '</h3>';
				echo '<ul>';
				$books = get_bookmarks( "category=$cat->cat_ID" );
				foreach ( $books as $book ) {
				echo '<li>';
				echo '<a href="' . $book->link_url . '">' . $book->link_name . '</a>';
				echo '</li>';
				} // end books loop
				echo '</ul>';
				} // end cats loop;
				?>
				</div><!-- end of link categories -->


            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>
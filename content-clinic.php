<?php
/**
 * Default Content Template
 *
 * This template is the default content template. It is used to display the content of a
 * template file, when no more specific content-*.php file is available.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
global $post;
// print_r( $post );
$phone_1 = get_post_meta( get_the_ID(), 'phone_1', true );
$phone_2 = get_post_meta( get_the_ID(), 'phone_2', true );
$phone_3 = get_post_meta( get_the_ID(), 'phone_3', true );
$fax = get_post_meta( get_the_ID(), 'fax', true );
$email = get_post_meta( get_the_ID(), 'email', true );
$days_open = get_post_meta( get_the_ID(), 'days_open', true );
$opening_time = get_post_meta( get_the_ID(), 'opening_time', true );
$closing_time = get_post_meta( get_the_ID(), 'closing_time', true );
$more_info = get_post_meta( get_the_ID(), 'more_info', true );
$address = get_post_meta( get_the_ID(), '_pronamic_google_maps_address', true );

$title_before = '<h2 class="title">';
$title_after = '</h2>';

if ( ! is_single() ) {
	$title_before = $title_before . '<a href="' . get_permalink( get_the_ID() ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
	$title_after = '</a>' . $title_after;
}

$page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

woo_post_before();
?>
<article <?php post_class(); ?> itemscope itemtype="http://schema.org/MedicalClinic">
<?php
	woo_post_inside_before();
?>
	<header>
		<?php the_title( $title_before, $title_after ); ?>
	</header>
	<div class="post-meta">
		<?php echo clinic_meta(); ?>
	</div>
	<?php if ( !empty($phone_1) || !empty($days_open) || !empty($address) ) { ?>
		<section class="fields">
		    <?php if ( !empty( $phone_1 ) ) { ?>
		    	<li>
		    		<i class="fa fa-phone"></i>
		    		<span itemprop="telephone"><?php echo $phone_1; ?></span>
		    		<?php if ( !empty( $phone_2 ) ) { ?>
		    			<span itemprop="telephone"><span class="pipe"> | </span><?php echo $phone_2; ?></span>
		    		<?php }; ?>
		    		<?php if ( !empty( $phone_3 ) ) { ?>
		    			<span itemprop="telephone"><span class="pipe"> | </span><?php echo $phone_3; ?></span>
		    		<?php } ?>
		    	</li>
		    <?php } ?>
		    <?php if ( !empty( $fax ) ) { ?>
		    	<li>
		    		<i class="fa fa-print"></i>
		    		<span itemprop="faxNumber"><?php echo $fax; ?></span>		    		
		    	</li>
		    <?php } ?>
		    <?php if ( !empty( $email ) ) { ?>
		    	<li>
		    		<i class="fa fa-envelope"></i>
		    		<span itemprop="email"><?php echo $email; ?></span>		    		
		    	</li>
		    <?php } ?>
		    <?php if ( !empty( $days_open ) ) { ?>
		    	<li>
		    		<i class="fa fa-clock-o"></i>
		    		<span itemprop="openingHours"><?php echo $days_open; ?></span>
		    		<?php if ( !empty( $opening_time ) ) { ?>
		    			<span itemprop="openingHours"><?php echo '<span class="pipe"> | </span>' . $opening_time; ?></span>
		    		<?php } ?>
		    		<?php if ( !empty( $closing_time ) ) { ?>
		    			<span itemprop="openingHours"><?php echo ' - ' . $closing_time; ?></span>
		    		<?php } ?>

		    	</li>
		    <?php } ?>
		    <?php if ( !empty( $more_info ) ) { ?>
		    	<li>
		    		<i class="fa fa-info"></i>	    		
		    		<span><?php echo $more_info; ?></span>
		    	</li>
		    <?php } ?>
		    <?php if ( !empty( $address ) ) { ?> 
		    	<li>
		    		<i class="fa fa-home"></i>
		    		<span itemprop="address"><?php echo $address; ?></span>
		    	</li>
		    <?php } ?>
		    <?php if ( !empty( $post->post_content ) && $post->post_content != "&nbsp;" ) { ?> 
			    <li>
			    	<i class="fa fa-stethoscope"></i>		    	
			    	<span itemprop="availableService"><?php the_content(); ?></span>
			    </li>
		    <?php } ?>
		</section><!-- /.fields -->
	<?php } ?>

	<?php if ( is_single() ) {
		if ( function_exists( 'pronamic_google_maps' ) ) {
		    pronamic_google_maps( array(
		        'width'  => 635,
		        'height' => 400,
		        'map_options' => array(
		        	'zoom' => 15
		        )
		    ) );
		}
	} ?>
<?php
	// woo_post_inside_after();
?>
</article><!-- /.post -->
<?php
	woo_post_after();
	// comments_template();
	if ( is_single() )	
		echo related_content( 'post', 'district', 5, 'Related Stories' );
?>
<?php

class healthe_latest_post extends WP_Widget {

    /** constructor */
    function healthe_latest_post() {
        parent::WP_Widget(false, $name = 'Health-e Latest Post');	
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {	
        extract( $args );
        $title 			= apply_filters('widget_title', $instance['title']);
        $cat 			= apply_filters('widget_title', $instance['cat']);                

        echo $before_widget;
        if ( $title )
            echo $before_title . $title . $after_title;

        $posts = get_posts( "category=$cat&posts_per_page=1" );

        foreach ( $posts as $latest_post ) { 
            ?>
            <a href="<?php echo get_permalink( $latest_post->ID ); ?>"><?php echo get_the_post_thumbnail( $latest_post->ID, 'sidebar' ); ?></a>            
            <div class="copy">
                <h4><a href="<?php echo get_permalink( $latest_post->ID ); ?>"><?php echo get_the_title( $latest_post->ID ); ?></a></h4>
                <?php echo $latest_post->post_excerpt; ?>
            </div>
            <?php
        }

        echo $after_widget;
        
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {		
		global $posttypes;
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['cat'] = strip_tags($new_instance['cat']);
    
    return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {	

		
	
    $title = esc_attr($instance['title']);
    $cat = esc_attr($instance['cat']);
    ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category ID'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('cat'); ?>" name="<?php echo $this->get_field_name('cat'); ?>" type="text" value="<?php echo $cat; ?>" />
        </p>		
        <?php 
    }


}

add_action('widgets_init', create_function('', 'return register_widget("healthe_latest_post");'));

?>
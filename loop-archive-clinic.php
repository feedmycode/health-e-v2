<?php
/**
 * Loop - Archive
 *
 * This is the loop logic used on all archive screens.
 *
 * To override this loop in a particular archive type (in all categories, for example), 
 * duplicate the `archive.php` file and rename the duplicate to `category.php`.
 * In the code of `category.php`, change `get_template_part( 'loop', 'archive' );` to 
 * `get_template_part( 'loop', 'category' );` and save the file.
 *
 * Create a duplicate of this file and rename it to `loop-category.php`.
 * Make any changes to this new file and they will be reflected on all your category screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
 global $more; $more = 0;
 
if ( get_post_type() == 'clinic' ) {
 		?>
 		<div class="breadcrumb breadcrumbs woo-breadcrumbs">
 			<div class="breadcrumb-trail">
 				<span class="trail-before">
 					<span class="breadcrumb-title">You are here:</span>
 				</span> 
 				<a href="<?php echo home_url(); ?>" title="Health-e" rel="home" class="trail-begin">Home</a> 
 				<span class="sep">&gt;</span>
 				<a href="/south-african-health-facilities/">Health Facilities</a>
 				<span class="sep">&gt;</span>
 				<?php 
 				$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );			
 				?>
 				<span class="trail-end"><?php echo $term->name ?></span>
 			</div>
 		</div>
 		<?php
} else {
	woo_loop_before();
};

if (have_posts()) { $count = 0;

	$title_before = '<h1 class="archive_header">';
	$title_after = '</h1>';
	
	woo_archive_title( $title_before, $title_after );
	
	// Display the description for this archive, if it's available.
	woo_archive_description();
?>

<?php if ( function_exists( 'pronamic_google_maps_mashup' ) && get_post_type() == 'clinic' ) { ?>
	<img class="geomap-loader" src="<?php echo get_stylesheet_directory_uri() . '/images/loader-128.gif' ;?>" alt="loader" />
	<div class="geomap">		
		<?php
			pronamic_google_maps_mashup(
			    array(
			        'post_type' => get_post_type(),
			        'taxonomy' => get_query_var('taxonomy'),
			        'term' => get_query_var('term'),
			        'posts_per_page' => '-1'
			    ), 
			    array(
			        'width'          => 635,
			        'height'         => 460, 
			        'nopaging'       => true,
			        'map_type_id'    => 'roadmap', 
			        'marker_options' => array(
			            'icon' => $map_icon
			        )
			    )
			);
		?>
	</div>
<?php } ?>

<div class="fix"></div>

<?php	
	while (have_posts()) { the_post(); $count++;
		woo_get_template_part( 'content', get_post_type() );
	} // End WHILE Loop */	
} else {
	get_template_part( 'content', 'noposts' );
} // End IF Statement

woo_loop_after();

woo_pagination('mid_size=2');
?>
<script>
jQuery(document).ready(function($) {
  $(window).on('load', function() {
    $('.geomap').css('visibility', 'visible');
    $('.geomap-loader').hide();
  })
});
</script>
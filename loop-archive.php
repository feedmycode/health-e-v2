<?php
/**
 * Loop - Archive
 *
 * This is the loop logic used on all archive screens.
 *
 * To override this loop in a particular archive type (in all categories, for example), 
 * duplicate the `archive.php` file and rename the duplicate to `category.php`.
 * In the code of `category.php`, change `get_template_part( 'loop', 'archive' );` to 
 * `get_template_part( 'loop', 'category' );` and save the file.
 *
 * Create a duplicate of this file and rename it to `loop-category.php`.
 * Make any changes to this new file and they will be reflected on all your category screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
 global $more; $more = 0;
 
if ( ! is_home() ) woo_loop_before();

if (have_posts()) { $count = 0;

	$title_before = '<h1 class="archive_header">';
	$title_after = '</h1>';
	
	woo_archive_title( $title_before, $title_after );
	
	// Display the description for this archive, if it's available.
	woo_archive_description();
?>

<div class="fix"></div>

<?php
	if ( is_home() ) {
		$featured_posts = get_posts( array(
				'posts_per_page' => 1,				
				'tax_query' => array(
					array(
						'taxonomy' => 'post_tag',
						'field' => 'slug',
						'terms' => 'featured'
					)
				) 
			)
		);
		foreach ( $featured_posts as $featured ) {

			$post_formats = get_post_format( $featured->ID );
			if( false != $post_formats ) {
				if( is_array() ){
					$post_formats = implode( ' ', $post_formats );
				}
			}else{
				$post_formats = '';
			}

			if ( 'video' == $post_formats ) {
				$icon = '<a href="' . get_permalink( $featured->ID ) . '"><i class="fa fa-video-camera"></i></a>';
			} else if ( 'audio' == $post_formats ) {
				$icon = '<a href="' . get_permalink( $featured->ID ) . '"><i class="fa fa-headphones"></i></a>';
			} else {	
				//if standard posts needs one.
				$icon = '';
			}
			?>
			<div class="featured">
				<div class="image">
					<a href="<?php echo get_permalink( $featured->ID ); ?>"><?php echo get_the_post_thumbnail( $featured->ID, 'home-featured' ); ?></a>
				</div>
				<div class="copy">
			    	<h2><a href="<?php echo get_permalink( $featured->ID ); ?>"><?php echo limit($featured->post_title, 10); ?></a></h2>
			    	<p><?php echo limit( $featured->post_excerpt, 20 ); ?></p>
			    	<?php $cat_name = get_the_category( $featured->ID )[0]->name; ?>
			    	<?php $cat_id = get_the_category( $featured->ID )[0]->term_id; ?>
			    	<?php $cat_link = get_category_link( $cat_id ); ?>
			    	<a class="article-cat" href="<?php echo $cat_link; ?>"><?php echo $cat_name; ?></a>
			    	<ul class="related">
				    	<?php
				    	$related_count = 0;

				    	// Related Content (Posts to Posts connections)
				    	if ( class_exists( 'P2P_Autoload' ) ) {    					    	
					    	$related_posts = get_posts( array(
						    	  'connected_type' => 'posts_to_posts',
						    	  'connected_items' => $featured->ID,
						    	  'nopaging' => true,
						    	  'suppress_filters' => false,
					    		) 
					    	);
				    	}

				    	// Related Content (Category Fallback)
				    	if ( ! $related_posts ) {					    	
					    	$related_posts = get_posts( array(
										    		"posts_per_page" 	=> 3,
										    		"category" 			=> $cat_id,
										    		"post__not_in" 		=> array($featured->ID)
										    		)
										    	);
						}

				    	foreach ( $related_posts as $related ) {
				    		$related_count++;
				    		?>
				    		<li><a href="<?php echo get_permalink( $related->ID ); ?>" title="<?php echo $related->post_title; ?>"><?php echo limit($related->post_title, 7); ?></a></li>
				    		<?php
				    		if ( $related_count >= 3 ) break;
				    	}
				    	?>
			    	</ul>
			    </div>
			    <?php echo $icon; ?>
			</div>
			<?php
		}
		?>		
		<?php
	}
?>

<div class="fix"></div>

<?php
	while (have_posts()) { the_post(); $count++;
		
		$post_formats = get_post_format();
		if( false != $post_formats ) {
			if( is_array() ){
				$post_formats = implode( ' ', $post_formats );
			}
		}else{
			$post_formats = '';
		}

		if ( 'video' == $post_formats ) {
			$icon = '<a href="' . get_permalink() . '"><i class="fa fa-video-camera"></i></a>';
		} else if ( 'audio' == $post_formats ) {
			$icon = '<a href="' . get_permalink() . '"><i class="fa fa-headphones"></i></a>';
		} else {	
			//if standard posts needs one.
			$icon = '';
		}
		
		if ( $count <= 4 ) {			

			if ( $count%2 != 0 ) 
				echo 
			"<article class='post-block large fl'>";
			else
				echo 
			"<article class='post-block large fr'>";
			?>			
				<div class="image">
					<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( 'archive-large' ); ?></a>														
					<?php $cat_name = get_the_category( get_the_id() )[0]->name; ?>
					<?php $cat_id = get_the_category( get_the_id() )[0]->term_id; ?>
					<?php $cat_link = get_category_link( $cat_id ); ?>
					<a class="article-cat" href="<?php echo $cat_link; ?>"><?php echo $cat_name; ?></a>
				</div>
				<div class="copy">
					<h3><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo limit( get_the_title(), 9 ); ?></a></h3>
					<p><?php echo limit( get_the_excerpt(), 20 ); ?></p>
				</div>
				<?php echo $icon; ?>
			</article>
			<?php if ( $count%2 == 0 ) echo "<div class='fix'></div>";

		} else {

			if ( $count%2 != 0 ) 
				echo 
			"<article class='post-block fl'>";
			else
				echo 
			"<article class='post-block fr'>";
			?>			
				<div class="image">
					<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail( 'archive' ); ?></a>																			
				</div>
				<div class="copy">
					<h3><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php echo limit( get_the_title(), 9 ); ?></a></h3>
					<p><?php echo limit( get_the_excerpt(), 20 ); ?></p>
				</div>
				<?php echo $icon; ?>
			</article>
			<?php if ( $count%2 == 0 ) echo "<div class='fix'></div>";
		}

	} // End WHILE Loop
} else {
	get_template_part( 'content', 'noposts' );
} // End IF Statement

woo_loop_after();

woo_pagination('mid_size=2');
?>
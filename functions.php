<?php

// Include Latest Post Widget
include( get_stylesheet_directory() . '/includes/widgets/healthe-latest-post.php' );

// Add Image Sizes
add_image_size('home-featured', 385, 248, true);
add_image_size('archive-large', 310, 145, true);
add_image_size('archive', 145, 145, true);
add_image_size( 'sidebar', 300, 135, true);

// Register Footer Nav Location
add_action( 'init', 'register_footer_menu' );
function register_footer_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}

// Add support for post formats
add_action( 'after_setup_theme', 'health_e_formats', 11 );
function health_e_formats(){
     add_theme_support( 'post-formats', array( 'audio','video' ) );
}

// Enqeueu JS and CSS

add_action( 'init', 'healthe_enqueues' );
function healthe_enqueues() {
  wp_enqueue_script( 'leanModal', get_stylesheet_directory_uri() . '/js/jquery.leanModal.min.js', array( 'jquery' ) );
  wp_enqueue_script( 'video-js', get_stylesheet_directory_uri() . '/js/video-js/video.min.js' );
  wp_enqueue_style( 'video-js-css', get_stylesheet_directory_uri() . '/js/video-js/video-js.min.css' );
}


// Header Extras
add_action( 'woo_header_inside', 'healthe_header_inside' );
function healthe_header_inside() {
    ?>
    <div class="header-content">
    <div class="tagline">          
          <h2>The South African Health News Service</h2>
    </div>
    <div class="header-buttons">
          <a class="button" rel="leanModal" href="#subscribe-modal">Subscribe</a>
          <a class="button" rel="leanModal" href="#contact-modal">Contact</a>
    </div>
    </div>
    <?php
}

// Exclude featured post from main loop on home
add_action('pre_get_posts', 'exclude_featured_home');
function exclude_featured_home( $query ) {  
    if ( $query->is_home() && $query->is_main_query() ) {
        $featured_posts = get_posts( array(
                'posts_per_page' => 1,              
                'tax_query' => array(
                    array(
                        'taxonomy' => 'post_tag',
                        'field' => 'slug',
                        'terms' => 'featured'
                    )
                ) 
            )
        );     
        foreach ( $featured_posts as $featured ) 
            $exclude_id = $featured->ID; //Save ID in order to exclude from main loop
        $query->set( 'post__not_in', array( $exclude_id ) );
    }
}

// List PDF in Media Manager
add_filter( 'post_mime_types', 'modify_post_mime_types' );
function modify_post_mime_types( $post_mime_types ) {

	// select the mime type, here: 'application/pdf'
	// then we define an array with the label values

	$post_mime_types['application/pdf'] = array( __( 'PDFs' ), __( 'Manage PDFs' ), _n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' ) );

	// then we return the $post_mime_types variable
	return $post_mime_types;

}
 
// Word Count Limiter
function limit($input, $limit) {
    
    $output = explode( ' ', $input, $limit );
    
    if ( count( $output ) >= $limit ) {
        array_pop( $output );
        $output = implode( ' ', $output ) . '...';
    } else {
        $output = implode( ' ', $output );
    }

    $output = preg_replace( '`\[[^\]]*\]`', '', $output );
    return $output;
}

// Attachment Icons
function attachment_icon( $type ) {
  switch ($type) {
    case 'application/pdf':
        echo '<img class="icon" src="' . get_stylesheet_directory_uri() . '/images/file-types/default.png" />';
        break;

    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        echo '<img class="icon" src="' . get_stylesheet_directory_uri() . '/images/file-types/doc.jpg" />';
        break;
    
    case 'image/jpeg':
    case 'image/jpg':
    case 'image/png':
        echo '<img class="icon" src="' . get_stylesheet_directory_uri() . '/images/file-types/image.jpg" />';
        break;

    default:
        echo '<img class="icon" src="' . wp_mime_type_icon( $type ) . '" />';
        break;
    }

}

// Include Thumbnails in RSS Feed
add_filter('the_excerpt_rss', 'insertThumbnailRSS');
add_filter('the_content_feed', 'insertThumbnailRSS');
function insertThumbnailRSS($content) {
    global $post;
    if ( has_post_thumbnail( $post->ID ) ){
        $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
        $content = '<p><img src="' . $img[0] . '" /></p>' . $content;
    }
    return $content;
}

// Remove Author
add_action( 'after_setup_theme', 'remove_woo_author' );
function remove_woo_author() {
    remove_action( 'wp_head', 'woo_author', 10);
}

add_action( 'wp_head', 'woo_coauthor', 10);
function woo_coauthor() {
    // Author box single post page
    if ( is_single() )
        add_action( 'woo_post_inside_after', 'woo_coauthor_box', 10 ); 
    
    if ( is_author() ) 
        add_action( 'woo_loop_before', 'woo_coauthor_box', 10);
    
}

function woo_coauthor_box() {
    global $post;

    if ( is_single() && function_exists( 'get_coauthors' ) ) {
        $coauthors = get_coauthors( $post->ID );
    }

    if ( is_author() ) {
        $coauthors[] = $post->post_author; 
    }

    $count = 0; 

    ?> 
    <aside id="post-author"> <?php

    foreach ( $coauthors as $coauthor ) {
        $count++;
        if ( $count % 2 == 0 ) {
            ?> <div class="profile-block alt"> <?php
        } else {
            ?> <div class="profile-block"> <?php
        }
        ?>
        <div class="profile-image"><?php echo get_avatar( $coauthor->ID, '80'); ?></div>
        <div class="profile-content">
            <h4><?php printf( esc_attr__( 'About %s', 'woothemes' ), get_the_author_meta( 'display_name', $coauthor->ID ) ); ?></h4>
            <?php echo get_the_author_meta( 'description', $coauthor->ID ); ?>
            <?php if ( is_singular() ) { ?>
            <div class="profile-link">
                <a href="<?php echo esc_url( get_author_posts_url( $coauthor->ID ) ); ?>">
                    <?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'woothemes' ), get_the_author_meta( 'display_name', $coauthor->ID ) ); ?>
                </a>
            </div><!--#profile-link-->
        <?php } ?>
        </div><!--#profile-content-->
        </div><!--#profile-block-->
        <div class="fix"></div>
        <?php
        }

    ?> </aside> <?php

}

// Clinic Meta
function clinic_meta() {

    // Taxonomies
    $province = get_the_terms( get_the_ID(), 'province' );
    $district = get_the_terms( get_the_ID(), 'district' );
    $city = get_the_terms( get_the_ID(), 'city' );
    $suburb = get_the_terms( get_the_ID(), 'suburb' );
    $clinic_type = get_the_terms( get_the_ID(), 'clinic_type' );      

    if ( !empty( $province ) || !empty( $district ) || !empty( $suburb ) ) {
        $meta .= '<span itemscope itemtype="http://schema.org/PostalAddress">';
        $meta .= '<span class="tax-title">Location</span>'; 
        foreach ( $suburb as $term ) {
            $term_link = get_term_link( $term, 'suburb' );
            $meta .= '<a itemprop="addressLocality" class="location" href="' . $term_link . '">' . $term->name . '</a>, ';
        }
        foreach ( $district as $term ) {
            $term_link = get_term_link( $term, 'district' );
            $meta .= '<a itemprop="addressRegion" class="district" href="' . $term_link . '">' . $term->name . '</a>, ';
        }
        foreach ( $province as $term ) {
            $term_link = get_term_link( $term, 'province' );
            $meta .= '<a class="province" href="' . $term_link . '">' . $term->name . '</a>';
        }
        $meta .= "</span>";
        $meta .= "<br/>";
    }

    if ( !empty( $city ) ) {          
        $meta .= get_the_term_list( get_the_ID(), 'city', '<span class="tax-title">Cities</span>', ', ', '' );
        $meta .= "<br/>";
    }

    if ( !empty( $clinic_type ) ) {          
        $meta .= get_the_term_list( get_the_ID(), 'clinic_type', '<span class="tax-title">Clinic Type</span>', ', ', '' );
        $meta .= "<br/>";
    }

    return $meta;
}

// Clinic Archive Ordering
add_action( 'pre_get_posts', 'clinic_archive_order' );
function clinic_archive_order( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_post_type_archive( 'clinic' ) || is_tax( 'province' ) || is_tax( 'district' ) || is_tax( 'suburb' ) || is_tax( 'clinic_type' ) ) {
        $query->set( 'post_type', 'clinic' );
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
        $query->set( 'post_parent', 0 );
        return;
    }
}

// Related Content
function related_content( $post_type, $taxonomy, $numposts, $title = "Related Content" ) {
    $terms = get_the_terms( get_the_ID(), $taxonomy );
    foreach ( $terms as $term ) {
        $term_list[] = $term->slug;
    }
    $args = array(
      'post_type' => $post_type,      
      'posts_per_page' => $numposts,
      'tax_query' => array(
          array(
            'taxonomy' => $taxonomy,
            'field' => 'slug',
            'terms' => $term_list
          )
        )
    );
    $related_query = new WP_Query( $args );
    if ( $related_query->have_posts() ) : 
        $output .= '<div class="related-content"><h3>' . $title . '</h3><ul>';
        while ( $related_query->have_posts() ) : $related_query->the_post();
            $output .= "<li>";
            $output .= "<a href='" . get_permalink() . "'>" . get_the_title() . "</a>";
            if ( get_post_type() == 'post' )
                $output .= "<span class='post-meta'> - " . get_the_date() . ", " . get_the_time() . "</span>";
            $output .= "</li>";
        endwhile;
        $output .= "</ul></div>";
    endif;
    wp_reset_query();
    return $output;
}

// Posts to Posts Connection

function healthe_connections() {
    p2p_register_connection_type( array(
        'name' => 'posts_to_posts',
        'from' => 'post',
        'to' => 'post',
        'reciprocal' => true
    ) );
}
add_action( 'p2p_init', 'healthe_connections' );




// Search

add_action( 'woo_nav_inside', 'nav_extras' );
function nav_extras() {
  ?>
    <div class="header-social fr">
        <a target="_blank" href="https://www.youtube.com/channel/UChHeoyV3LvHlZfZLMhPQ8xg"><i class="fa fa-youtube"></i></li>
        <a target="_blank" href="https://twitter.com/HealtheNews"><i class="fa fa-twitter"></i></a>        
        <a target="_blank" href="https://www.facebook.com/pages/Health-e-News-Service/184242111654185"><i class="fa fa-facebook"></i></a>
        <a target="_blank" href="<?php echo home_url(); ?>/feed/"><i class="fa fa-rss"></i></a>
    </div>
    <form method="get" class="searchform" action="<?php home_url(); ?>">
        <input type="text" class="field s" name="s" value="Search..." onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}">
        <button type="submit" class="fa fa-search submit" name="submit" value="Search"></button>
    </form>    
  <?php
}




add_action( 'woo_footer_top', 'footer_menu' );
function footer_menu() {
     /**
    * Displays a navigation menu
    * @param array $args Arguments
    */
    $args = array(
        'theme_location' => 'footer-menu',
        'menu' => '',
        'container' => 'nav',
        'container_class' => 'col-full',
        'container_id' => 'footer-navigation',
        'menu_class' => 'nav fl',
        'menu_id' => 'main-nav',
        'echo' => true,
        'fallback_cb' => 'wp_page_menu',
        'before' => '',
        'after' => '',
        'link_before' => '',
        'link_after' => '',
        'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
        'depth' => 0,
        'walker' => ''
    );
  
    wp_nav_menu( $args );
}

add_action( 'woo_footer_after', 'modals' );
function modals() {
    ?>
    <script>
        jQuery(document).ready(function($) {
            $(function() {
                $('a[rel*=leanModal]').leanModal({ top : 50, closeButton: ".modal_close" });   
            });
        });        
    </script>
    <div id="subscribe-modal">
        <?php gravity_form( 5 ); ?>
        <a class="modal_close"><i class="fa fa-times"></i></a>
    </div>
    <div id="contact-modal">
        <?php gravity_form( 6 ); ?>
        <a class="modal_close"><i class="fa fa-times"></i></a>
    </div>
    <?php
}


// Custom shortcode function for coauthors post links

function coauthor_links_shortcode( ) {

	if ( function_exists( 'coauthors_posts_links' ) ) {
		$author = coauthors_posts_links( ',', null, null, null, false );
	} else {
		$author = '[post_author_posts_link]';
	}
	return  $author;
}
add_shortcode('author_links','coauthor_links_shortcode');
function woo_post_meta() {

	if ( is_page() && !( is_page_template( 'template-blog.php' ) || is_page_template( 'template-magazine.php' ) ) ) {
		return;
	}

	$post_info = '<span class="small">' . __( 'By', 'woothemes' ) . '</span> [author_links] <span class="small">' . _x( 'on', 'post datetime', 'woothemes' ) . '</span> [post_date] <span class="small">' . __( 'in', 'woothemes' ) . '</span> [post_categories before=""] ';
	printf( '<div class="post-meta">%s</div>' . "\n", apply_filters( 'woo_filter_post_meta', $post_info ) );

} // End woo_post_meta()
<?php
/**
 * Single Post Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a post ('post' post_type).
 * @link http://codex.wordpress.org/Post_Types#Post
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>
       
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">                       
<?php
	// 
	if ( get_post_type() == 'clinic' ) {
		?>
		<div class="breadcrumb breadcrumbs woo-breadcrumbs">
			<div class="breadcrumb-trail">
				<span class="trail-before">
					<span class="breadcrumb-title">You are here:</span>
				</span> 
				<a href="<?php echo home_url(); ?>" title="Health-e" rel="home" class="trail-begin">Home</a> 
				<span class="sep">&gt;</span>
				<a href="/south-african-health-facilities/">Health Facilities</a>
				<span class="sep">&gt;</span>
				<?php echo get_the_term_list( get_the_ID(), 'province', '', '', '<span class="sep"> &gt; </span>'); ?>
				<?php echo get_the_term_list( get_the_ID(), 'district', '', '', '<span class="sep"> &gt; </span>'); ?>
				<?php echo get_the_term_list( get_the_ID(), 'city', '', '', '<span class="sep"> &gt; </span>'); ?>
				<?php echo get_the_term_list( get_the_ID(), 'suburb', '', '', '<span class="sep"> &gt; </span>'); ?>
				<span class="trail-end"><?php echo get_the_title(); ?></span>
			</div>
		</div>
		<?php
	} else {
		woo_loop_before();
	}
	?>
	<?php 
	
	if (have_posts()) { $count = 0;
		while (have_posts()) { the_post(); $count++;
			
			woo_get_template_part( 'content', get_post_type() ); // Get the post content template file, contextually.
		}
	}
	
	woo_loop_after();
?>     
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar('alt'); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>